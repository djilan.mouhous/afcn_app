import * as React from 'react';
import { ListView, ListViewBase, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import SingleSite from '../components/Site';
import TacheTable from '../components/TacheTable';

import { Text, View } from '../components/Themed';
import { Site, Tache } from '../types';

export default function TabOneScreen() {
  let clientName = "Djilan"
  let sites : Array<Site>= [
    {
      name : "Mon site web",
      sub : ["SEM", "Pratique"],
      quota: 4.3,
      id : 3
    },
    {
      name : "Mon deuxième site",
      sub : ["SEM", "Pratique"],
      quota: 3,
      id : 4
    }
  ]
  let taches : Array<Tache> = [
    {
      name : "Exemple de Tâche",
      id: "2",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "5",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "6",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "7",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "8",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "9",
      time : 324,
      status : 'draft'
    },
    {
      name : "Exemple de Tâche",
      id: "10",
      time : 324,
      status : 'pending'
    },
    {
      name : "Exemple de Tâche",
      id: "11",
      time : 324,
      status : 'pending'
    },
    {
      name : "Exemple de Tâche",
      id: "12",
      time : 324,
      status : 'pending'
    },
    {
      name : "Exemple de Tâche",
      id: "13",
      time : 324,
      status : 'pending'
    },
    {
      name : "Exemple de Tâche",
      id: "14",
      time : 324,
      status : 'pending'
    },
    {
      name : "Exemple de Tâche",
      id: "15",
      time : 324,
      status : 'publish'
    },
    {
      name : "Exemple de Tâche",
      id: "16",
      time : 324,
      status : 'publish'
    },
    {
      name : "Exemple de Tâche",
      id: "17",
      time : 324,
      status : 'publish'
    },
    {
      name : "Exemple de Tâche",
      id: "18",
      time : 324,
      status : 'publish'
    },
    {
      name : "Exemple de Tâche",
      id: "19",
      time : 324,
      status : 'publish'
    },
    {
      name : "Exemple de Tâche",
      id: "20",
      time : 324,
      status : 'publish'
    },
  ]
  let content = {}
  return (
    <ScrollView  contentContainerStyle={content} >
      <View  style={styles.container}>
      <Text style={styles.title}>Bienvenue {clientName}</Text>
      <Text style={styles.subTitle}>Gérez tous vos sites et vos tâches directement sur votre smartphone !</Text>
      <Text style={styles.titleH2}>Mes sites</Text>
      <View  style={styles.sitesContainer}>
      <ScrollView horizontal={true}>
      {sites.map((site) =>{
        return (
          <SingleSite site={site} />
        )
      })}
      </ScrollView>
      </View>
      <Text style={styles.titleH2}>Mes tâches</Text>
      <View  style={styles.tachesContainer}>
        <TacheTable taches={taches}/>
      </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingBottom: 30
  },
  title: {
    marginTop: 30,
    marginBottom: 10,
    fontSize: 30,
    fontWeight: 'bold',
  },
  sitesContainer:{
    height: 230,
  },
  tachesContainer:{
    height: 300,
    flex: 1,
  },
  titleH2: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 17,
    fontWeight: 'normal',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
