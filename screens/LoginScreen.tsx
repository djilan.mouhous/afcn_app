import * as React from 'react';
import { Button, StyleSheet, useColorScheme } from 'react-native';
import { TextInput, TouchableHighlight } from 'react-native-gesture-handler';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import Colors from '../constants/Colors';
import { LoginType } from '../types';


export default function Login(props: {tryToLog : (a:LoginType)=>Boolean}) {
    const [username, onChangeUsername] = React.useState('username');
    const [password, onChangePassword] = React.useState('password');
    let theme = useColorScheme()
    let backgroundColor = ""
    theme == "dark" ? backgroundColor = Colors.dark.text : backgroundColor = Colors.light.text
    
    return (
    <View style={styles.container}>
      <Text style={styles.title}>Connexion</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: backgroundColor, width: 200, paddingHorizontal:10, marginVertical: 10 }}
        onChangeText={text => onChangeUsername(text)}
        value={username}
      />
      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1 , backgroundColor: backgroundColor, width: 200, paddingHorizontal:10, marginVertical: 10  }}
        onChangeText={text => onChangePassword(text)}
        value={password}
      />
      <Button
        onPress={() => {props.tryToLog({id:username,password:password})}}
        title="Valider"
        color="#841584"
        accessibilityLabel="Learn more about this purple button"
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
