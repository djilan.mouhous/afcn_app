import React from 'react';
import { Pressable, StyleSheet, useColorScheme } from 'react-native';
import { Text,View } from './Themed';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Tache } from '../types';
import TacheLines from './TacheLines';
import Colors from '../constants/Colors';

export default function TacheTable(props: {taches : Array<Tache>}) {

    let tacheComplete = props.taches.filter(tache => tache.status == 'publish')
    let tacheProcessing = props.taches.filter(tache => tache.status == 'pending')
    let tachePending = props.taches.filter(tache => tache.status == 'draft')
    
    const [activeTab, setActiveTab] = React.useState('draft')
    let tachesToRender
    switch (activeTab) {
        case 'draft':
            tachesToRender = tachePending
            break;
        case 'pending':
            tachesToRender = tacheProcessing
            break;
        case 'publish':
            tachesToRender = tacheComplete
            break;
        default:
            tachesToRender = props.taches
            break;
    }
    const theme = useColorScheme();
    let bgTab
    theme == 'dark' ? bgTab = Colors.light.tint : Colors.dark.tint
    return (
        
        <View 
        style={styles.box} 
        darkColor="rgba(255,255,255,0.1)"
        lightColor="rgba(0,0,0,0.1)"
        >            
            <View
            style={styles.tab}
            darkColor="rgba(255,255,255,0.1)"
            lightColor="rgba(0,0,0,0.1)">
                <Pressable onPress={()=>setActiveTab('draft')} style={activeTab == 'draft' ? {...styles.tab_single,...{backgroundColor: bgTab}} : styles.tab_single }>
                    <Text
                      lightColor="rgba(0,0,0,0.8)"
                      darkColor="rgba(255,255,255,0.8)">
                    En attente</Text>
                </Pressable>
                <Pressable onPress={()=>setActiveTab('pending')} style={activeTab == 'pending' ? {...styles.tab_single,...{backgroundColor: bgTab} } : styles.tab_single }>
                    <Text
                      lightColor="rgba(0,0,0,0.8)"
                      darkColor="rgba(255,255,255,0.8)">
                    En cours</Text>
                </Pressable>
                <Pressable onPress={()=>setActiveTab('publish')} style={activeTab == 'publish' ? {...styles.tab_single,...{backgroundColor: bgTab} } : styles.tab_single }>
                    <Text
                      lightColor="rgba(0,0,0,0.8)"
                      darkColor="rgba(255,255,255,0.8)">
                    Terminées</Text>
                </Pressable>
            </View>
        <View style={{backgroundColor: 'transparent', height: 300, flex: 1}}>

            <TacheLines taches={tachesToRender}/>
        </View>
        </View>
    );
}

const styles = StyleSheet.create({
    box:{
        borderRadius: 16,
        paddingHorizontal: 10,
        paddingVertical: 20,
        width: '100%',
        flex: 1,
    },
    tab:{
        flexDirection: 'row',
        
        marginBottom : 20, 
        borderRadius: 18,
        justifyContent: 'space-between',
        
    },
    tab_single:{
        height: 40,
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderRadius: 18,
        alignItems: 'center'
    },


  });
  