import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Text,View } from './Themed';
import { Tache } from '../types';
import { FlatList } from 'react-native-gesture-handler';
import TacheLineSingle from './TacheSingle';

export default function TacheLines(props: {taches : Array<Tache>}) {

    const renderItem = (props : { item : Tache }) => (
        <TacheLineSingle tache={props.item} />
      );
    
    return (
        <FlatList
        data={props.taches}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        />
    );
}

const styles = StyleSheet.create({
    tacheLine:{
        borderRadius: 16,
        paddingVertical: 10,
        paddingHorizontal: 30,
        width: '100%',
        marginBottom: 10,
        flex: 1,
        flexDirection: 'row'
    },
    title:{
        fontSize: 16,
        fontWeight: 'bold'
    },
    h2:{
        marginTop: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    h3:{
        marginTop: 5,
        fontSize: 14,
    },
    flexSite: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 20,
        justifyContent: 'space-between'
    },
    textSiteBlock:{
        backgroundColor: 'transparent'
    },
    button: {
        alignItems: "center",
        padding: 10
    },
    goToSite:{
        
    }
  });
  