import React from 'react';
import { Site } from '../types';
import { StyleSheet, useColorScheme } from 'react-native';
import { Text,useThemeColor,View } from './Themed';
import Colors from '../constants/Colors';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

export default function SingleSite(props: {site : Site}) {
    const navigation = useNavigation();
    let theme = useColorScheme()
    let backgroundColor = ""
    theme == "dark" ? backgroundColor = Colors.dark.background2 : backgroundColor = Colors.light.background2
    return (
        
        <View 
        style={styles.box} 
        darkColor="rgba(255,255,255,0.1)"
        lightColor="rgba(0,0,0,0.1)"
        >
        <Text 
        style={styles.title} 
        lightColor="rgba(0,0,0,0.8)"
        darkColor="rgba(255,255,255,0.8)">
            {props.site.name}
        </Text>
        <View style={styles.flexSite}>
            <View style={styles.textSiteBlock}>
                <Text 
                style={styles.h2} 
                lightColor="rgba(0,0,0,0.8)"
                darkColor="rgba(255,255,255,0.8)">
                    Abonnements :
                </Text>
                {props.site.sub.map((s) =>{
                    return(
                        <Text 
                        key={s}
                        style={styles.h3} 
                        lightColor="rgba(0,0,0,0.8)"
                        darkColor="rgba(255,255,255,0.8)">
                            {s}
                        </Text>
                    )
                })}
            </View>
            <View style={styles.textSiteBlock}>
                <Text 
                style={styles.h2} 
                lightColor="rgba(0,0,0,0.8)"
                darkColor="rgba(255,255,255,0.8)">
                    Temps restant :
                </Text>
                <Text 
                style={styles.h3} 
                lightColor="rgba(0,0,0,0.8)"
                darkColor="rgba(255,255,255,0.8)">
                    {props.site.quota} h
                </Text>
            </View>
        </View>
        <TouchableHighlight onPress={() => navigation.navigate("SingleSiteScreen", props.site)}>
        <View style={styles.button}
        lightColor="rgba(0,0,0,0.1)"
        darkColor="rgba(255,255,255,0.1)">
        
          <Text>CONSULTER</Text>
        </View>
      </TouchableHighlight>
        </View>
    );
}

const styles = StyleSheet.create({
    box:{
        height: 230,
        borderRadius: 16,
        padding: 30,
        width: 300,
        
        marginRight: 30
    },
    title:{
        fontSize: 16,
        fontWeight: 'bold'
    },
    h2:{
        marginTop: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    h3:{
        marginTop: 5,
        fontSize: 14,
    },
    flexSite: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 20,
        justifyContent: 'space-between'
    },
    textSiteBlock:{
        backgroundColor: 'transparent'
    },
    button: {
        alignItems: "center",
        padding: 10
    },
    goToSite:{
        
    }
  });
  