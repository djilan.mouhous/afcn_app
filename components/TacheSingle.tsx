import React from "react";
import { Tache } from "../types";
import { View, Text } from "./Themed";
import {StyleSheet} from 'react-native'
import Colors from "../constants/Colors";

export default function TacheLineSingle(props: {tache: Tache}){
   
    let color : string
    switch (props.tache.status) {
        case 'draft':
            color = '#7f8c8d'
            break;
        case 'publish':
            color = '#27ae60'
            break;
        case 'pending':
            color = '#e67e22'
            break;
        default:
            color = '#e67e22'
            break;
    }
    return(
    <View style={styles.tacheLine}  darkColor="rgba(255,255,255,0.1)" lightColor="rgba(0,0,0,0.1)">
        <View style={{backgroundColor: color, height: 30, width: 15, marginRight: 20, borderRadius: 20}}></View>
        <Text>{props.tache.name}</Text>
    </View>)
}

const styles = StyleSheet.create({
    tacheLine:{
        borderRadius: 16,
        paddingVertical: 10,
        paddingHorizontal: 30,
        width: '100%',
        marginBottom: 10,
        flex: 1,
        flexDirection: 'row'
    },
    title:{
        fontSize: 16,
        fontWeight: 'bold'
    },
    h2:{
        marginTop: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    h3:{
        marginTop: 5,
        fontSize: 14,
    },
    flexSite: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        marginBottom: 20,
        justifyContent: 'space-between'
    },
    textSiteBlock:{
        backgroundColor: 'transparent'
    },
    button: {
        alignItems: "center",
        padding: 10
    },
    goToSite:{
        
    }
  });