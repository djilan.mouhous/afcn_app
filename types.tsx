export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Accueil: undefined;
  SingleSite: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  TabThree: undefined;
  SingleSite: undefined;
  TabFour: undefined;
  TabFive: undefined;
};

export type Site = {
  name: string,
  id: number,
  quota: number,
  sub: Array<string>
}
export type Tache = {
  name: string,
  id: string,
  time: number,
  status: string
}

export type TabOneParamList = {
  TabOneScreen: undefined;
  SingleSiteScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type LoginType = {
  id: string,
  password: string
}