import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import Login from './screens/LoginScreen';
import { LoginType } from './types';
import * as SecureStore from 'expo-secure-store';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();
  const [isLogin, setisLogin] = React.useState(false)
  //SecureStore.deleteItemAsync('secure_token') //This line clean the local storage 
  SecureStore.getItemAsync('secure_token')
  .then((token) => {
    console.log(token)
    setisLogin(token != null ? true : false)
  })
  
function handleLogin (data : LoginType){
    if (data.id == 'username' && data.password == "password"){
      setisLogin(true)
      SecureStore.setItemAsync('secure_token','thisIsARandomToken')
        .then(() => {
          SecureStore.getItemAsync('secure_token')
          .then((token) => {
            console.log(token)
            return true
          })
        })
      return false
    } else{
      setisLogin(false)
      return false
    }
    
  }
  console.log("My login", isLogin)
  if (!isLoadingComplete) {
    return null;
  } else if (isLogin == true) {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  } else {
    return (
      <SafeAreaProvider>
        <Login tryToLog={handleLogin}/>
      </SafeAreaProvider>
    )
    }
}
